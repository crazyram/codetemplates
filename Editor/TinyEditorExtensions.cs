﻿#if CRAZY_EXTENSIONS_INCLUDED
using System;
using System.Linq;
using CrazyRam.Core.Helpers;
using Unity.Mathematics;
using UnityEngine;
using UnityEditor;

public static class TinyEditorExtensions
{
    [MenuItem("CrazyRam/Editor/Parent/To center")]
    public static void ParentToCenter()
    {
        var objects = Selection.gameObjects;
        if (!objects.IsNullOrEmpty())
            ParentToCenter(objects, FindCenter);
    }

    [MenuItem("CrazyRam/Editor/Parent/To XZ center ")]
    public static void ParentToCenterXz()
    {
        var objects = Selection.gameObjects;
        if (!objects.IsNullOrEmpty())
        {
            ParentToCenter(objects, gameObjects =>
            {
                var center = FindCenter(gameObjects);
                var commonBounds = FindBoundsValue(gameObjects);
                return new float3(center.x, commonBounds.center.y - commonBounds.extents.y, center.z);
            });
        }
    }

    private static void ParentToCenter(GameObject[] objects, Func<GameObject[], float3> centerSearch)
    {
        var center = centerSearch(objects);

        var root = new GameObject($"{objects[0].name}Root");
        root.transform.position = center;
        Undo.RegisterCreatedObjectUndo(root, "Parent root creation");
        for (int i = 0; i < objects.Length; i++)
            Undo.SetTransformParent(objects[i].transform, root.transform, "Root to center");
        Selection.activeGameObject = root;
    }

    private static float3 FindValue(GameObject[] objects, float initialStorage, Func<float3, float3, float3> comparand)
    {
        return objects.Aggregate(new float3(initialStorage),
            (storage, item) => comparand(storage, item.transform.position));
    }

    private static Bounds FindBoundsValue(GameObject[] objects)
    {
        if (objects.IsNullOrEmpty())
            throw new NullReferenceException("Objects are null");
        var initialBounds = new Bounds(objects[0].transform.position, Vector3.zero);
        return objects.Aggregate(initialBounds,
            (storage, item) =>
            {
                var copy = storage;
                FindObjectBounds(item, ref copy);
                return copy;
            });
    }

    private static void FindObjectBounds(GameObject go, ref Bounds storage)
    {
        if (!go)
            return;
        var renderer = go.GetComponent<Renderer>();
        if (!renderer)
            return;
        storage.Encapsulate(renderer.bounds);
        for (int i = 0; i < go.transform.childCount; i++)
            FindObjectBounds(go.transform.GetChild(i).gameObject, ref storage);
    }

    private static float3 FindCenter(GameObject[] objects)
    {
        return FindCenter(objects, out _, out _);
    }

    private static float3 FindCenter(GameObject[] objects, out float3 min, out float3 max)
    {
        min = FindValue(objects, float.MaxValue, math.min);
        max = FindValue(objects, float.MinValue, math.max);
        return math.lerp(min, max, 0.5f);
    }
}
#endif
