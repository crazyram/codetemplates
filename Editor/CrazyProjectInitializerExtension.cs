﻿using System.IO;
using System.Linq;
using UnityEditor;

namespace CrazyRam.Editor
{
    public static class CrazyProjectInitializerExtension
    {
        private const int MenuItemPriority = 60;

        private const string ModulesFileName = ".gitmodules";

        private const string BaseModulePath = "Assets/Programming/CrazyPlugins";

        private const string ModuleScheme = @"[submodule ""{0}""]
    path = {0}
    url = {1}
";

        private static (string url, string path)[] Modules => new[]
        {
            ("git@bitbucket.org:crazyram/extensions.git", $"{BaseModulePath}/Extensions"),
            ("git@bitbucket.org:crazyram/crazymessaging.git", $"{BaseModulePath}/CrazyMessaging"),
            ("git@bitbucket.org:crazyram/crazyframework.git", $"{BaseModulePath}/CrazyFramework"),
            ("git@bitbucket.org:crazyram/initer.git", $"{BaseModulePath}/Initer"),
            ("git@bitbucket.org:crazyram/localizationsystem.git", $"{BaseModulePath}/LocalizationSystem"),
            ("git@bitbucket.org:crazyram/serializedinterface.git", $"{BaseModulePath}/SerializedInterface")
        };

        [MenuItem("Assets/CrazyRam/InitCrazyProject", false, MenuItemPriority)]
        public static void Initialize()
        {
            if (File.Exists(ModulesFileName))
                return;
            var content = string.Concat(Modules.Select((data) => string.Format(ModuleScheme, data.path, data.url)));
            File.WriteAllText(ModulesFileName, content);
        }
    }
}
