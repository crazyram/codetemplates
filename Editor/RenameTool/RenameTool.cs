﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RenameTool : EditorWindow
{
    private readonly List<GameObject> _objects = new List<GameObject>();

    private bool _expand;

    private string _baseName;

    [MenuItem("CrazyRam/Editor/Rename Tool %g")]
    public static void Open()
    {
        var window = GetWindow<RenameTool>();
        window.Show();
        window.CheckSelected();
    }

    private void CheckSelected()
    {
        _objects.AddRange(Selection.gameObjects);
    }

    public void OnGUI()
    {
        DrawList();
        ProcessDragAndDrop();
        ProcessButtons();
    }

    private void DrawList()
    {
        _expand = EditorGUILayout.Foldout(_expand, "Expand");
        if (_expand)
        {
            foreach (var gameObject in _objects)
            {
                if (gameObject != null)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(gameObject.name);
                    if (GUILayout.Button("X"))
                        _objects.Remove(gameObject);
                    EditorGUILayout.EndHorizontal();
                }
            }
        }
    }

    private void ProcessButtons()
    {
        _baseName = EditorGUILayout.TextField("Base name: ", _baseName);
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Apply"))
        {
            for (int i = 0; i < _objects.Count; i++)
            {
                var go = _objects[i];
                if (go != null)
                    go.name = string.Format("{0}{1}", _baseName, i);
            }
        }
        EditorGUILayout.Space();
        if (GUILayout.Button("Clear"))
            _objects.Clear();
        EditorGUILayout.EndHorizontal();
    }

    private void ProcessDragAndDrop()
    {
        var e = Event.current;
        switch (e.type)
        {
            case EventType.DragUpdated:
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                break;
            case EventType.DragPerform:
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                DragAndDrop.AcceptDrag();
                foreach (var o in DragAndDrop.objectReferences)
                {
                    var go = o as GameObject;
                    if (go == null || _objects.Contains(go))
                        continue;
                    _objects.Add(go);
                }
                break;
        }
    }
}
