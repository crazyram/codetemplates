﻿using UnityEditor;
using UnityEngine;

public static class CrazyTemplates
{
    private static readonly Texture2D ScriptIcon = (EditorGUIUtility.IconContent("cs Script Icon").image as Texture2D);

    private const string MenuItemPath = "Assets/Create/Extend/";

    private const int MenuItemPriority = 60;

    private const string TemplatesPath = "Assets/Editor/CrazyTemplates/";

    [MenuItem(MenuItemPath + "C# Class", false, MenuItemPriority)]
    public static void CreateClass()
    {
        CreateSharpFile("NewClassTemplate.cs", TemplatesPath + @"Class.txt");
    }

    [MenuItem(MenuItemPath + "C# Struct", false, MenuItemPriority)]
    public static void CreateStruct()
    {
        CreateSharpFile("NewStructTemplate.cs", TemplatesPath + @"Struct.txt");
    }

    [MenuItem(MenuItemPath + "C# Enum", false, MenuItemPriority)]
    public static void CreateEnum()
    {
        CreateSharpFile("NewEnumTemplate.cs", TemplatesPath + @"Enum.txt");
    }

    [MenuItem(MenuItemPath + "C# Interface", false, MenuItemPriority)]
    public static void CreateInterface()
    {
        CreateSharpFile("NewInterfaceTemplate.cs", TemplatesPath + @"Interface.txt");
    }

    [MenuItem(MenuItemPath + "C# ScriptableObject", false, MenuItemPriority)]
    public static void CreateScriptableObjectTemplate()
    {
        CreateSharpFile("NewScriptableObjectTemplate.cs", TemplatesPath + @"ScriptableObject.txt");
    }

    [MenuItem("Assets/CrazyRam/CreateFileHierarchy", false, MenuItemPriority)]
    public static void CreateFileHierarchy()
    {
        ExtendSharpTemplates.CreateFileHierarchy();
    }

    private static void CreateSharpFile(string newPath, string templatePath)
    {
        ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<CrazyCreateFile>(),
            newPath, ScriptIcon, templatePath);
    }
}
