using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;

namespace CrazyRam.Editor
{
    public static class PackagesInitializerExtension
    {
        private static Request _activeRequest;

        private static int _waitCycles;

        private static int _requestId = -1;

        private const int MenuItemPriority = 60;

        private const int DefaultWaitCycles = 10;

        private static readonly string[] BasePackages =
        {
            "com.unity.mathematics",
            "com.unity.burst",
            "com.unity.collections",
            "com.unity.device-simulator",
            "com.unity.jobs"
        };

        [MenuItem("Assets/CrazyRam/InitDefaultPackages", false, MenuItemPriority)]
        public static void Init()
        {
            if (_activeRequest != null && _requestId != -1)
                return;
            _requestId = 0;
            RequestNextPackage();
            EditorApplication.update += OnEditorUpdate;
        }

        private static void RequestNextPackage()
        {
            EditorApplication.LockReloadAssemblies();
            _activeRequest = Client.Add(BasePackages[_requestId]);
            _waitCycles = DefaultWaitCycles;
        }

        private static void OnEditorUpdate()
        {
            if (_activeRequest is {IsCompleted: false})
                return;
            if (_waitCycles > 0)
            {
                _waitCycles--;
                return;
            }

            Debug.Log($"Package {BasePackages[_requestId]} install status: {_activeRequest.Status}");
            _requestId++;
            if (_requestId < BasePackages.Length)
            {
                RequestNextPackage();
                return;
            }

            Debug.Log($"Installed ${BasePackages.Length} packages");
            _requestId = -1;
            EditorApplication.update -= OnEditorUpdate;

            EditorApplication.UnlockReloadAssemblies();
        }
    }
}
